package com.joyce;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
@EnableHystrix
public class UserApplication {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserApplication.class);
	
	@Bean
	@LoadBalanced  //负载均衡，默认为轮询策略
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	/* 据说是turbine有用的代码，但不知做啥用
	@Bean
	public ServletRegistrationBean getServletRegistrationBean() {
		HystrixMetricsStreamServlet servlet = new HystrixMetricsStreamServlet();
		ServletRegistrationBean bean = new ServletRegistrationBean(servlet);
		bean.setLoadOnStartup(1);
		bean.addUrlMappings("/hystrix.stream");
		bean.setName("HystrixMetricsStreamServlet");
		return bean;
	}
	*/

	public static void main(String[] args) { 
		SpringApplication.run(UserApplication.class, args); 
	}

}
