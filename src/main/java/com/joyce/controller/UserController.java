package com.joyce.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class UserController {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserController.class);
	
	@Value("${server.port}")
	private String serverPort;
	@Value("${spring.application.name}")
	private String spingApplicationName;
	@Autowired
	private RestTemplate restTemplate;

	@RequestMapping(value="/userSayHi")
	public String userSayHiMethod(String name) {
		LOG.info("/userSayHi userSayHiMethod...");
		return " Hello " + name+", " +spingApplicationName+":"+serverPort;
	}
	@RequestMapping(value="/orderName")
	public String orderNameMethod(String name) {
		LOG.info("/orderName orderNameMethod...");
		return " Hello " + name+", " +spingApplicationName+":"+serverPort ;
	}
}
